package net.zimad_test.domain.model;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
