package net.zimad_test.domain.model;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public class DataResult<T> {
    private Status responseType;
    private T data;
    private Throwable error;

    public DataResult(Status responseType, T data, Throwable error) {
        this.responseType = responseType;
        this.data = data;
        this.error = error;
    }

    public Status getResponseType() {
        return responseType;
    }

    public T getData() {
        return data;
    }

    public Throwable getError() {
        return error;
    }
}
