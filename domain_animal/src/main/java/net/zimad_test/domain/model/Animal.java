package net.zimad_test.domain.model;

/**
 * Created by Burmaka V on 28.05.2019.
 */
public class Animal {
    private String name;
    private String imageUrl;

    public String getName() {
        return name;
    }

    public Animal(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
