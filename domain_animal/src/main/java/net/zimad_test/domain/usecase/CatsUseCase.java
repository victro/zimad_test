package net.zimad_test.domain.usecase;

import net.zimad_test.domain.AnimalCallback;
import net.zimad_test.domain.Repository;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public class CatsUseCase {
    Repository repository;

    public CatsUseCase(Repository repository) {
        this.repository = repository;
    }

    public void getCats(AnimalCallback callback){
        new Thread(() -> callback.onExecute(repository.getCats())).start();
    }
}
