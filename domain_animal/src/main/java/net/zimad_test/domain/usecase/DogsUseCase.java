package net.zimad_test.domain.usecase;

import net.zimad_test.domain.AnimalCallback;
import net.zimad_test.domain.Repository;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public class DogsUseCase {
    Repository repository;

    public DogsUseCase(Repository repository) {
        this.repository = repository;
    }

    public void getDogs(AnimalCallback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                callback.onExecute(repository.getDogs());
            }
        }).start();
    }
}
