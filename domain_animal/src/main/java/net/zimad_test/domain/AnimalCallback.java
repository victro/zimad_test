package net.zimad_test.domain;

import net.zimad_test.domain.model.Animal;
import net.zimad_test.domain.model.DataResult;

import java.util.List;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public interface AnimalCallback {
    void onExecute(DataResult<List<Animal>> result);
}
