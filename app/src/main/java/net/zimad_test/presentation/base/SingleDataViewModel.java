package net.zimad_test.presentation.base;

import android.app.Application;

import net.zimad_test.R;
import net.zimad_test.domain.model.DataResult;
import net.zimad_test.domain.model.Status;
import net.zimad_test.utils.SingleLiveEvent;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

/**
 * Created by Burmaka V on 30.05.2019.
 */
public abstract class SingleDataViewModel<T> extends AndroidViewModel {

    public final SingleLiveEvent<T> successEvent = new SingleLiveEvent<>();
    public final SingleLiveEvent<Boolean> progressEvent = new SingleLiveEvent<>();
    public final SingleLiveEvent<String> errorEvent = new SingleLiveEvent<>();

    public SingleDataViewModel(@NonNull Application application) {
        super(application);
    }

    protected void handleResponse(DataResult<T> response) {
        Status resultStatus = response.getResponseType();
        switch (resultStatus) {
            case LOADING:
                progressEvent.postValue(true);
                break;
            case ERROR:
                progressEvent.postValue(false);
                errorEvent.postValue(response.getError().getMessage());
                break;
            case SUCCESS:
                progressEvent.postValue(false);
                successEvent.postValue(response.getData());
                break;
            default:
                progressEvent.postValue(false);
                errorEvent.postValue(getApplication().getString(R.string.unknown_error));
                break;
        }
    }
}
