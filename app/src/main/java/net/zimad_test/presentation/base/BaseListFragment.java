package net.zimad_test.presentation.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.zimad_test.R;
import net.zimad_test.domain.model.Animal;
import net.zimad_test.presentation.adapter.AnimalsRecyclerViewAdapter;
import net.zimad_test.presentation.view.AnimalDetailsActivity;
import net.zimad_test.presentation.view.DataLoadingInterface;
import net.zimad_test.presentation.adapter.OnAnimalListClickListener;

import java.util.List;

public abstract class BaseListFragment extends Fragment implements DataLoadingInterface, OnAnimalListClickListener {

    public BaseListFragment() {
    }

    TextView emptyList;
    ProgressBar progressBar;
    RecyclerView listView;
    private AnimalsRecyclerViewAdapter adapter;

    protected abstract void onSetupView(View view);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_list, container, false);
        emptyList = view.findViewById(R.id.tvBaseListEmpty);
        progressBar = view.findViewById(R.id.pbBaseList);
        listView = view.findViewById(R.id.rvBaseList);

        listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AnimalsRecyclerViewAdapter(this);
        listView.setAdapter(adapter);

        onSetupView(view);
        showEmptyScreen();
        return view;
    }

    protected void updateList(List<Animal> animalList) {
        adapter.updateList(animalList);
    }

    public void onAnimalClicked(int position, Animal item) {
        routeToDetails(position, item);
    }

    private void routeToDetails(int position, Animal item) {
        Intent intent = new Intent(getActivity(), AnimalDetailsActivity.class);
        intent.putExtra(AnimalDetailsActivity.ARG_NAME, item.getName());
        intent.putExtra(AnimalDetailsActivity.ARG_IMAGE_URL, item.getImageUrl());
        intent.putExtra(AnimalDetailsActivity.ARG_POSITION, position);

        startActivity(intent);
    }
    public void showProgress(boolean show) {
        if (show) {
            listView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            listView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            emptyList.setVisibility(View.GONE);
        }
    }

    public void showEmptyScreen() {
        listView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        emptyList.setVisibility(View.VISIBLE);
    }

    public void onError(String errorMessge) {
        Toast.makeText(getActivity(), errorMessge, Toast.LENGTH_SHORT).show();
    }

    public void onSuccess() {
        listView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        emptyList.setVisibility(View.GONE);
    }
}
