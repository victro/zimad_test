package net.zimad_test.presentation.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.zimad_test.R;
import net.zimad_test.presentation.viewmodel.AnimalDetailsViewModel;

public class AnimalDetailsActivity extends AppCompatActivity {
    public static final String ARG_NAME = "ARG_NAME";
    public static final String ARG_IMAGE_URL = "ARG_IMAGE_URL";
    public static final String ARG_POSITION = "ARG_POSITION";
    private final Picasso picasso = Picasso.get();


    AnimalDetailsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_details);

        viewModel = ViewModelProviders.of(this).get(AnimalDetailsViewModel.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            viewModel.init(extras.getString(ARG_NAME), extras.getString(ARG_IMAGE_URL), extras.getInt(ARG_POSITION));
        }

        TextView tvAnimalNumber = (TextView) findViewById(R.id.tvAnimalDetailsNumber);
        TextView tvAnimalName = (TextView) findViewById(R.id.tvAnimalDetailsName);
        ImageView ivAnimalImage = (ImageView) findViewById(R.id.ivAnimalDetailsImage);

        tvAnimalNumber.setText(String.valueOf(viewModel.getPosition()));
        tvAnimalName.setText(viewModel.getName());
        picasso.load(viewModel.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_placeholder)
                .into(ivAnimalImage);

    }
}
