package net.zimad_test.presentation.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import net.zimad_test.R;
import net.zimad_test.presentation.viewmodel.MainActivityViewModel;

import static net.zimad_test.presentation.viewmodel.MainActivityViewModel.FIRST_TAB_INDEX;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    TabLayout mainTabLayout;
    MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mainTabLayout = (TabLayout) findViewById(R.id.mainTabLayout);

        TabLayout.Tab catsTab = mainTabLayout.newTab();
        catsTab.setText(getString(R.string.tab_title_cats));

        mainTabLayout.addTab(catsTab);

        TabLayout.Tab dogsTab = mainTabLayout.newTab();
        dogsTab.setText(getString(R.string.tab_title_dogs));
        mainTabLayout.addTab(dogsTab);

        mainTabLayout.addOnTabSelectedListener(this);
        if (viewModel.getCurrentTab() == FIRST_TAB_INDEX) {
            catsTab.select();
        } else {
            dogsTab.select();
        }
        changeTab(viewModel.getCurrentTab());
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        changeTab(tab.getPosition());
    }

    private void changeTab(int tabPosition) {
        viewModel.setCurrentTab(tabPosition);
        Fragment fragment = null;
        if (tabPosition == FIRST_TAB_INDEX) {
            fragment = CatsListFragment.getInstance();
        } else {
            fragment = DogsListFragment.getInstance();
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.mainFragmentContainer, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onDestroy() {
        if (mainTabLayout != null) {
            mainTabLayout.removeOnTabSelectedListener(this);
        }
        super.onDestroy();
    }
}
