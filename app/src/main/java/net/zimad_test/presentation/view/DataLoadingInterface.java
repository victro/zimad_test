package net.zimad_test.presentation.view;

/**
 * Created by Burmaka V on 28.05.2019.
 */
public interface DataLoadingInterface {
    void showProgress(boolean show);
    void showEmptyScreen();
    void onError(String errorMessge);
    void onSuccess();
}
