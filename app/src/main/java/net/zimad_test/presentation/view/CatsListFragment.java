package net.zimad_test.presentation.view;

import android.view.View;

import net.zimad_test.presentation.viewmodel.CatsViewModel;
import net.zimad_test.presentation.base.BaseListFragment;

import androidx.lifecycle.ViewModelProviders;

/**
 * Created by Burmaka V on 28.05.2019.
 */
public class CatsListFragment extends BaseListFragment {

    private static CatsListFragment instance;
    CatsViewModel viewModel;

    public static CatsListFragment getInstance() {
        if (instance == null) {
            instance = new CatsListFragment();
        }
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    protected void onSetupView(View view) {
        viewModel = ViewModelProviders.of(getActivity()).get(CatsViewModel.class);
        viewModel.successEvent.observe(this, animals -> {
            onSuccess();
            if (animals != null && animals.size() > 0) {
                updateList(animals);
            } else {
                showEmptyScreen();
            }
        });
        viewModel.progressEvent.observe(this, this::showProgress);
        viewModel.errorEvent.observe(this, this::onError);
    }
}
