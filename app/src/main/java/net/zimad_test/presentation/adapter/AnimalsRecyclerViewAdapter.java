package net.zimad_test.presentation.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.zimad_test.R;
import net.zimad_test.domain.model.Animal;

import java.util.List;


public class AnimalsRecyclerViewAdapter extends RecyclerView.Adapter<AnimalsRecyclerViewAdapter.ViewHolder> {

    private List<Animal> animalList;
    private final OnAnimalListClickListener mListener;
    private final Picasso picasso = Picasso.get();

    public AnimalsRecyclerViewAdapter(OnAnimalListClickListener listener) {

        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_animal, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.animal = animalList.get(position);
        holder.tvAnimalItemNumber.setText(String.valueOf(position));
        holder.tvAnimalItemName.setText(animalList.get(position).getName());
        picasso.load(animalList.get(position).getImageUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_placeholder)
                .into(holder.ivAnimalItemImage);
    }

    public void updateList(List<Animal> items) {
        animalList = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return animalList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvAnimalItemNumber;
        public final TextView tvAnimalItemName;
        public final ImageView ivAnimalItemImage;

        public Animal animal;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvAnimalItemNumber = (TextView) view.findViewById(R.id.tvAnimalItemNumber);
            tvAnimalItemName = (TextView) view.findViewById(R.id.tvAnimalItemName);
            ivAnimalItemImage = (ImageView) view.findViewById(R.id.ivAnimalItemImage);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        int imagePosition = getAdapterPosition() + 1;
                        mListener.onAnimalClicked(imagePosition, animal);
                    }
                }
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvAnimalItemName.getText() + "'";
        }
    }
}
