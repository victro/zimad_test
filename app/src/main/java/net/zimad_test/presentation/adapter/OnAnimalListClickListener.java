package net.zimad_test.presentation.adapter;

import net.zimad_test.domain.model.Animal;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public interface OnAnimalListClickListener {
    void onAnimalClicked(int position, Animal item);
}
