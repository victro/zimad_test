package net.zimad_test.presentation.viewmodel;

import android.app.Application;

import net.zimad_test.data.RepositoryImpl;
import net.zimad_test.domain.usecase.DogsUseCase;
import net.zimad_test.domain.model.Animal;
import net.zimad_test.presentation.base.SingleDataViewModel;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by Burmaka V on 28.05.2019.
 */
public class DogsViewModel extends SingleDataViewModel<List<Animal>> {

    private DogsUseCase dogsUseCase = new DogsUseCase(RepositoryImpl.getInstanse());

    public DogsViewModel(@NonNull Application application) {
        super(application);
    }

    public void onResume() {
        if (successEvent.getValue() == null || successEvent.getValue().isEmpty()) {
            progressEvent.postValue(true);
            dogsUseCase.getDogs(this::handleResponse);
        } else {
            successEvent.call();
        }
    }
}
