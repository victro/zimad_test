package net.zimad_test.presentation.viewmodel;

import android.app.Application;

import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

/**
 * Created by Burmaka V on 31.05.2019.
 */
public class AnimalDetailsViewModel extends AndroidViewModel {

    private String name;
    private String imageUrl;
    private int position;

    public AnimalDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(String name, String imageUrl, int position) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getPosition() {
        return position;
    }
}
