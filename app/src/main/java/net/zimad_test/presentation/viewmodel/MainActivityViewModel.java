package net.zimad_test.presentation.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

/**
 * Created by Burmaka V on 27.05.2019.
 */
public class MainActivityViewModel extends AndroidViewModel {

    public final static int FIRST_TAB_INDEX = 0;

    private int currentTab = 0;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public int getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(int currentTab) {
        this.currentTab = currentTab;
    }
}
