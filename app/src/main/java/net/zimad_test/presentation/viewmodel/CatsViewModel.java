package net.zimad_test.presentation.viewmodel;

import android.app.Application;

import net.zimad_test.data.RepositoryImpl;
import net.zimad_test.domain.usecase.CatsUseCase;
import net.zimad_test.domain.model.Animal;
import net.zimad_test.presentation.base.SingleDataViewModel;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by Burmaka V on 28.05.2019.
 */
public class CatsViewModel extends SingleDataViewModel<List<Animal>> {

    private CatsUseCase catsUseCase = new CatsUseCase(RepositoryImpl.getInstanse());

    public CatsViewModel(@NonNull Application application) {
        super(application);
    }

    public void onResume() {
        if (successEvent.getValue() == null || successEvent.getValue().isEmpty()) {
            progressEvent.postValue(true);
            catsUseCase.getCats(this::handleResponse);
        } else {
            successEvent.call();
        }
    }
}
