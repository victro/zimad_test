package net.zimad_test.data;

import net.zimad_test.data.mapper.AnimalResponseToAnimalsListMapper;
import net.zimad_test.data.model.AnimalResponse;
import net.zimad_test.domain.Repository;
import net.zimad_test.domain.model.Animal;
import net.zimad_test.domain.model.DataResult;
import net.zimad_test.domain.model.Status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Burmaka V on 29.05.2019.
 */
public class RepositoryImpl implements Repository {

    private static final String API_URL = "http://kot3.com/";
    private ZimadApi serviceApi;

    private static Repository instanse;

    public synchronized static Repository getInstanse() {
        if (instanse == null) {
            instanse = new RepositoryImpl();
        }
        return instanse;
    }

    private RepositoryImpl() {
        serviceApi = retrofit.create(ZimadApi.class);
    }

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API_URL)
            .client(getDebugLoggerClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private OkHttpClient getDebugLoggerClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Override
    public DataResult<List<Animal>> getCats() {

        Call<AnimalResponse> call = serviceApi.getCats();
        try {
            AnimalResponse response = call.execute().body();
            List<Animal> result = AnimalResponseToAnimalsListMapper.toDomainModel(response);
            return new DataResult<>(Status.SUCCESS, result, null);
        } catch (IOException e) {
            e.printStackTrace();
            return new DataResult<List<Animal>>(Status.ERROR, new ArrayList<>(), e);
        }
    }

    @Override
    public DataResult<List<Animal>> getDogs() {
        Call<AnimalResponse> call = serviceApi.getDogs();
        try {
            AnimalResponse response = call.execute().body();
            List<Animal> result = AnimalResponseToAnimalsListMapper.toDomainModel(response);
            return new DataResult<>(Status.SUCCESS, result, null);
        } catch (IOException e) {
            e.printStackTrace();
            return new DataResult<List<Animal>>(Status.ERROR, new ArrayList<>(), e);
        }
    }
}
