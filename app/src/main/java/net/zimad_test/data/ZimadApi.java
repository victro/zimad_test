package net.zimad_test.data;

import net.zimad_test.data.model.AnimalResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Burmaka V on 30.05.2019.
 */
public interface ZimadApi {
    @GET("xim/api.php?query=cat")
    Call<AnimalResponse> getCats();

    @GET("xim/api.php?query=dog")
    Call<AnimalResponse> getDogs();
}
