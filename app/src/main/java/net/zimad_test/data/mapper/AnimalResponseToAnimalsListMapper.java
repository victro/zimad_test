package net.zimad_test.data.mapper;

import net.zimad_test.data.model.AnimalResponse;
import net.zimad_test.data.model.DataItem;
import net.zimad_test.domain.model.Animal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Burmaka V on 30.05.2019.
 */
public class AnimalResponseToAnimalsListMapper {
    public static List<Animal> toDomainModel (AnimalResponse response) {
        List<Animal> result = new ArrayList<>();
        if (response != null) {
            List<DataItem> responseList = response.getData();
            if ( responseList != null) {
                Animal animal = null;
                for (DataItem item : responseList) {
                    animal = new Animal(item.getTitle(), item.getUrl());
                    result.add(animal);
                }
            }
        }
        return result;
    }
}
